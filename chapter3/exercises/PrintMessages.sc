class Msg(val id: Int, val parent: Option[Int], val txt: String)

// https://github.com/handsonscala/handsonscala/issues/3#issuecomment-757325017
// my struggle was using recursion with pattern matching AND keeping track of the level

def printMessages(messages: Array[Msg]): Unit = {
  val parents = messages.filter(_.parent.isEmpty)

  def innerPrint(innerMessages: Array[Msg], level: Int = 0): Unit = {
    for (msg <- innerMessages) {
      println(("  " * level) + s"#${msg.id} ${msg.txt}")

      val children = messages.filter(_.parent.contains(msg.id))
      innerPrint(children, level + 1)
    }
  }

  innerPrint(parents)
}

printMessages(Array(
  new Msg(0, None, "Hello"),
  new Msg(1, Some(0), "World"),
  new Msg(2, None, "I am Cow"),
  new Msg(3, Some(2), "Hear me moo"),
  new Msg(4, Some(2), "Here I stand"),
  new Msg(5, Some(2), "I am Cow"),
  new Msg(6, Some(5), "Here me moo, moo")
))
