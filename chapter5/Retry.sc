def retry[T](max: Int, delay: Int)(f: => T): T = {
  var tries = 0
  var delayInMilliseconds = delay
  var result: Option[T] = None

  while (result == None) {
    try { result = Some(f) }
    catch {case e: Throwable =>
      Thread.sleep(delayInMilliseconds)
      delayInMilliseconds += 2
      tries += 1
      if (tries > max) throw e
      else {
        println(s"failed, retry #$tries")
      }
    }
  }
  result.get
}
